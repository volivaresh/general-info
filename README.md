[Make a ssh keygen](https://www.ssh.com/ssh/keygen/)

[add keygen ssh](https://docs.github.com/es/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)

[Python Debugging - pdb](https://rukbottoland.com/blog/como-utilizar-pdb-el-debugger-de-python/)

[SRE Books - Google](https://landing.google.com/sre/resources/#practicesandprocesses)

[Google SRE](https://landing.google.com/sre/)

[Node JS Info](https://nodejs.org/es/docs/)

[React JS](https://es.reactjs.org/)

[Docker References](https://docs.docker.com/engine/reference/builder/)

[Create React app Deployment](https://create-react-app.dev/docs/deployment)      

[Forms Google's Best Practice](https://web.dev/sign-in-form-best-practices/)

[JavaScript ES6 Syntax and Feature Overview](https://www.taniarascia.com/es6-syntax-and-feature-overview/)

[React Redux Inmutability Guide](https://daveceddia.com/react-redux-immutability-guide/)

[ECMAScript 6 - Javascript](https://developer.mozilla.org/es/docs/Web/JavaScript)


## Cover Letter:

Hi, (address the person or department of the company),

Thanks for the job opportunity.

I have a lot of experience developing Python apps, and I am confident that I am the one for your project. Depending on the amount and diversity of the data to be parsed, the project will last between one week and one month.

Thank you in advance,

XXXX


